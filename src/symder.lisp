



(defun is-variable (x)
  (symbolp x) )
(defun is-number (x)
  (numberp x ) )
(defun =number (a b)
  (and (is-number a)
	   (is-number b)
	   (= a b) ) )
(defun same-variable (a b)
  (and (is-variable a)
	   (is-variable b)
	   (eq a b) ) )

(defconstant symbol-sum '+)
(defconstant symbol-product '*)
(defconstant symbol-exp 'exp)
(defconstant symbol-log 'log)
(defconstant symbol-pow '**)




(defstruct argslist
  numbers
  others
)
(defun argslist-make (l)
  (assert (listp l))
  (let ((numbers (list))
		(others  (list)) )
	(loop for x in l do
		(if (is-number x)
		  (push x numbers )
		  (push x others )
		)
	)
	(make-argslist :numbers numbers :others others)
  )
)
(defun argslist-make-with-transform (l numbers-function)
	(let ((alist (argslist-make l)))
	    (make-argslist :numbers (apply numbers-function (argslist-numbers alist))
					   :others (argslist-others alist))
	)
)






;;;; sum: (+ 1 2 3 4 x y z) // 1 + 2 + 3 ... + y + z
; TODO: optimizations:
; 3. flatten symbols, by making multiplication

(defun is-sum ( x )
       (eq symbol-sum (car x)) )
(defun make-sum-from-list-optimized_1 (sum-list)
  (assert (> (length sum-list)
			 0))
  (if (= (length sum-list) 1)
	(car sum-list) ; optimization: if only one argument given, then return it
	(let* ( ; optimization: summate all numbers and push them if non-zero
		   (alist (argslist-make sum-list))
		   (suma (apply #'+ (argslist-numbers alist) ))
		  )
	  (if (= suma 0)
		(append (list symbol-sum) 
				(argslist-others alist))
		(append (list symbol-sum) 
				(argslist-others alist)
				(list suma))
	  )
	)
  )
)
(defun make-sum-from-list-optimized_2 (sum-list)
  (assert (> (length sum-list)
			 0))
  (if (= (length sum-list) 1)
	(car sum-list) ; optimization: if only one argument given, then return it
	(let* ( ; optimization: summate all numbers and push them if non-zero
		   (alist (argslist-make-with-transform sum-list #'+))
		  )
	  (if (= 0 (argslist-numbers alist))
		(append (list symbol-sum)
				(argslist-others alist))
		(append (list symbol-sum)
				(argslist-others alist)
				(list (argslist-numbers alist)))
	  )
	)
  )
)
(defun make-sum-from-list-basic (sum-list)
	(append (list symbol-sum) sum-list) )
(defun make-sum-from-list (l)
  (make-sum-from-list-optimized_2 l))

(defun make-sum ( &rest args )
  (make-sum-from-list args) )
(defun sum-getargs ( x ) (cdr x))
(defun sum-deriv (x var)
  (assert (is-sum x))
  (assert (is-variable var))
  (make-sum-from-list 
	(map 'list
		 (lambda (e) (deriv e var))
		 (sum-getargs e) ) ) )


;;;; product: (* 1 2 3 x y z) // 1 * 2 * ... * y * z
; TODO optimizations:
(defun is-product ( x )
       (equal '* (car x)) )

(defun _make-product-from-list-basic (arglist)
	(append (list '*) arglist) )
(defun _make-product-from-list-optimized_1 (arglist)
  (if (member 0 arglist)
	0 ;optimization 0. if any argument is zero -> return zero
	(append (list '*) arglist) ) ) 
(defun _make-product-from-list-optimized_2 (arglist)
	(let ((alist (argslist-make-with-transform arglist #'*)) ) ; optimization 1. multiply numbers
		(cond
		  ((equal 0 (argslist-numbers alist) ) ; optimization: multiplication by 0 gives back 0, no matter what
		   0)
		  ((or (not (argslist-numbers alist))
			   (equal 1 (argslist-numbers alist))
		   )
		   (append (list '*)
				   (argslist-others alist))
		  )
		  (t (append (list '*)
					 (list (argslist-numbers alist))
					 (argslist-others alist) ))
		)
	)
)
(defun _make-product-from-list-optimized_3 (arglist)
	(let (numbers others
		  (alist (argslist-make-with-transform arglist #'*)) )
		(cond
		  ((equal 0 (argslist-numbers alist) )
		   0)
		  ((or (not (argslist-numbers alist))
			   (equal 1 (argslist-numbers alist))
		   )
		   (append (list '*)
				   (argslist-others alist))
		  )
		  (t (append (list '*)
					 (list (argslist-numbers alist))
					 (argslist-others alist) ))
		)
	)
)

(defun make-product-from-list (alist) (_make-product-from-list-optimized_2 alist))
(defun make-product ( &rest args )
  (make-product-from-list args) )
(defun product-getargs ( x ) (cdr x))
(defun product-deriv (x var)
  (assert (is-product x))
  (assert (is-variable var))
  (let  ((args (product-getargs x))
		 retval
		)
    (loop for i from 0 to (- (length args) 1) do
		(let* ((_args (copy-list args))
			   (derivated (nth i _args) )
			   (rest-of-args (remove derivated _args))
			  )
		  (push (apply #'make-product (deriv derivated var) rest-of-args)
				retval)
		)
	)
	
	(make-sum-from-list retval)
  )
)



;;;; exp (exp x) // e^x , exp(x)
(defun is-exp ( x )
  (and (consp x)
	   (eq 'exp (car x)) ) )
(defun _make-exp-basic ( x ) ; (exp x) // exp(x)
	(if (is-number x)
		(exp x)
	 (cons 'exp x ) ) )
(defun make-exp (x)
  (_make-exp-basic x) )
(defun exp-get-power (x)
  (assert (is-exp x))
  (cdr x) )
(defun exp-deriv (x var)
  (assert (is-exp x))
  (let ((x (exp-get-power x)))
	(cond
		((same-variable x var)	; d(exp x))/d(x) = (exp x)
		 (make-exp x))
		(t (make-product (make-exp x)
						 (deriv x var) ))
	)
  )
)


;;;; log (log x) // log(x) - natural logarithm
(defun is-log ( x )
  (and (consp x)
	   (eq 'log (car x)) ) )

(defun _make-log_basic ( x )
	(if (is-number x)
	  (log x)
	  (cons 'log x ) ) )
(defun make-log (x)
  (_make-log_basic x) )

(defun log-get-value (x)
  (assert (is-log x))
  (cdr x) )

(defun log-deriv (x var)
  (assert (is-log x))
  (let ((v (log-get-value x)))
	(cond
	  ((equal v var)
	   (make-pow v -1)
	  )
	  (t (error "unable to deriv log d~a/d~a" v var))
	)
  )
)



;;;; pow(power/exponentiation) : (** b p) // a ** b // a ^ b
(defun is-pow ( x )
  (and (listp x)
	   (equal '** (car x)) )
)
(defun _make-pow-basic ( b p )
  (list '** b p) )

(defun make-pow (b p)
  (_make-pow-basic b p) )

(defun pow-get-base (x)
  (assert (is-pow x))
  (nth 1 x) )
(defun pow-get-power (x)
  (assert (is-pow x))
  (nth 2 x) )
(defun pow-deriv (x var)
  (assert (is-pow x))
  (let ((b (pow-get-base  x))
		(p (pow-get-power x)) )
	(if (and (is-variable b)
			 (equal b var)
			 (is-number   p) )
	  (make-pow b (- p 1) )
	  (error "unable to deriv pow of form: d~a/d~a " x var))
  )
)
#||
DUPA CYCKI XDDD
|#







(defun deriv (expr var)
  (assert (is-variable var))
  (cond
	((is-number expr)
	 0
	)
	((is-variable expr)
	 (if (eq expr var)
	   1
	   0
	 )
	)
	((is-sum expr)
	 (make-sum-from-list (map 'list (lambda (e) (deriv e var)) (cdr expr)) ))
	((is-product expr)
	 (product-deriv expr var))
	((is-exp expr)
	 (exp-deriv expr var))
	((is-log expr)
	 (log-deriv expr var))
	((is-pow expr)
	 (pow-deriv expr var))
	
	(t (error "unable to derivate d(~a)d~a" expr var))
  )
)


















(defun print-deriv (expr var)
  (format t "~% d(~a)/d(~a) = ~a" expr var (deriv expr var)) )


(defun main ()
  (print-deriv 'x 'x)
  (print-deriv 'y 'x)
  (print-deriv '1 'x)
  (print-deriv (make-sum 'x) 'x)
  (print-deriv (make-sum 'x 'y 'z 'x) 'x)
  (print-deriv (make-sum 'x 6 'y 3 'x) 'x)
  (print-deriv (make-exp 1) 'x)
  (print-deriv (make-exp 'x) 'x)
  (print-deriv (make-exp (make-sum 'x 'y)) 'x)
  (print-deriv (make-product 5 'x 'y 'x 2 (make-product 'x 'y)) 'x)
  (print-deriv (make-pow 'x 2) 'x)
  (print-deriv (make-log 'x) 'x)
)



(main)

