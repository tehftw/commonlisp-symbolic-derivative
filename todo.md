

TODO:
1. add better handling of constants - add symbolic constants!
2. maybe create a special `(defstruct mathargs)` for holding arguments to associative functions? e. g. `(defstruct mathargs :constants :variables :others)`, where field `:constants` would be a list of constants(number and symbolic), `:variables` would be symbolic variables, `:others` would be a list of functions
3. optimization: flatten sums/products (done for numeric constants)
4. implement chain rule of derivation
5. overhaul how the main `(deriv ...)` function works - make a lookup/assoclist(something like that'
